""" module to process the dictionary data manipulation """


class DictProcessor:
    """ set attributes """
    NamesDictionary = {'Zac': '27', 'Hal': '40', 'Max': '63', 'Bob': 'older than dirt'}

    def __init__(self):
        print(f"New Dictionary object created;\n{self.NamesDictionary}")
        return

    def search(self, searchValue):
        self.searchValue = searchValue
        print(self.searchValue)
        try:
            name_to_display = self.NamesDictionary.get(self.searchValue)
            if name_to_display:
                # print(f"found {name_to_search}, age {name_to_display}")
                # response_label.config(text=f"found {self.searchValue}: {name_to_display}")
                return f"found {self.searchValue}: {name_to_display}"
                # messagebox_click(f"{self.searchValue}: {name_to_display}")
            else:
                return "That name is not found!"
                # print("That name is not found!")

        except:
            return "Error: exception occurred!"
            # print("Error: exception occurred!")

        # searchEntry.delete(0, "end")     # clear search entry


def main():
    myDictionary = DictProcessor()
    print(f"myDictionary is; {myDictionary.NamesDictionary}")



if __name__ == '__main__':
    main()

