# This is the dictionary data structures exercise
# on a single page GUI (no menu)

import tkinter as tk
from tkinter import messagebox  # import messagebox library
from DictProcessor import DictProcessor

# initialize a startup dictionary called NamesDictionary
# NamesDictionary = {'Zac': '27', 'Hal': '40', 'Max': '63'}
NamesDictionary = {
    'Zac': '27',
    'Hal': '40',
    'Max': '63',
    'Bob': 'older than dirt',
    'Ann': '99',
    'Sally': 'not tellin',
    'Hen3ry': '135'
}


def display_greeting():         # included in starter code
    return ("Welcome to the Names Dictionary\n"
            "program. This program stores some names\n"
            "and ages in a Dictionary type data structure.\n"
            "Create JSON by entering the key at the name\n"
            "input and the value at the age input" )


def main():
    myDictionary = DictProcessor()

    def add_submit():
        temp_first_name = name_entry.get().title()  # this is the key of key:value pair
        temp_age = age_entry.get()  # this is the value of key:value pair
        NamesDictionary[temp_first_name] = temp_age
        add_name_label.config(text=f"Added {temp_first_name}: {temp_age}")
        # tk.Label(add_window, text=f"Added {temp_first_name}, age {temp_age}").pack()
        name_entry.delete(0, "end")     # clear name entry
        age_entry.delete(0, "end")      # clear age entry
        view_dictionary()
        countNames.config(text=f"Number of names: {len(NamesDictionary)}")

    # messagebox popup controls
    def messagebox_click(display_msg):
        print("messagebox triggered")
        if messagebox.askyesno(title='ask yes or no', message=f'Do you want to delete entry? \n"{display_msg}"'):
            print('yes, delete :)')
            try:
                name_to_remove = NamesDictionary.pop(searchEntry.get().title())
                response_label.config(text=f"deleted {searchEntry.get()}: {name_to_remove}")
                countNames.config(text=f"Number of names: {len(NamesDictionary)}")
            except:
                response_label.config(text="Error: delete function exception!")
            searchEntry.delete(0, "end")
            view_dictionary()
        else:
            searchEntry.delete(0, "end")
            response_label.config(text=" ")
            print('keep it :(')

    # name_to_search = input("Enter name to search: ")
    def search_submit():
        response_label.config(text=myDictionary.search(searchEntry.get().title()))
        '''
        try:
            name_to_display = NamesDictionary.get(searchEntry.get().title())
            if name_to_display:
                # print(f"found {name_to_search}, age {name_to_display}")
                response_label.config(text=f"found {searchEntry.get().title()}: {name_to_display}")
                messagebox_click(f"{searchEntry.get().title()}: {name_to_display}")
            else:
                response_label.config(text="That name is not found!")
                # print("That name is not found!")

        except:
            response_label.config(text="Error: exception occurred!")
            # print("Error: exception occurred!")
        '''

        # searchEntry.delete(0, "end")     # clear search entry

    def view_dictionary():
        # a widget with scrolling would be next upgrade
        view_text.delete("1.0", "end")
        print("refresh text")
        # for loop to show each item on its own line, or maybe use listbox
        for k, v in NamesDictionary.items():
            # tk.Label(view_window, text=f"{k}, age; {v}").pack()
            view_text.insert("end", f"{k}: {v}\n")
        # tk.Label(view_window, text=NamesDictionary).pack()

        return

    window = tk.Tk()
    window.geometry("400x600")
    window.title("Dictionary Data GUI")
    window.config(background="light green")
    icon = tk.PhotoImage(file='m3-blue-icon.png')
    window.iconphoto(True, icon)

    tk.Label(window, text=display_greeting(), bg="light green", font="none 12").pack(pady=10)

    tk.Label(window, text="Enter name to search:", bg='light green').pack()
    searchIcon = tk.PhotoImage(file='icons8-search-48.png')
    searchFrame = tk.Frame(window, bg='light green', bd=1, relief='solid')
    searchFrame.pack()
    searchLabel = tk.Label(searchFrame, image=searchIcon, bg='light green')
    searchLabel.pack(side="left")
    searchEntry = tk.Entry(searchFrame, font='none 16', width=16)
    searchEntry.pack(side="left")
    searchButton = tk.Button(searchFrame, text="Go", command=search_submit)
    searchButton.pack(side="right", padx=8)
    response_label = tk.Label(window, text=" ")
    response_label.pack()

    tk.Label(window, text="\nHere is the entire contents of the dictionary:", bg="light green").pack()

    view_frame = tk.Frame(window)
    view_frame.pack()
    view_text = tk.Text(view_frame,
                        height=5,  # set height as number of lines
                        width=27,
                        font="none, 14")
    view_text.pack(side="left", expand=True)
    sb = tk.Scrollbar(view_frame, orient="vertical", bg="green", width=16)
    sb.pack(side="right", fill="y")
    view_text.config(yscrollcommand=sb.set)
    sb.config(command=view_text.yview)
    view_dictionary()

    countNames = tk.Label(window,
                          text=f"Number of names: {len(NamesDictionary)}",
                          bg='light green',
                          font="none, 12"
                          )
    countNames.pack(pady=3)

    tk.Label(window, text="\nAdd contents to the dictionary:", bg="light green").pack()

    # use frames to put label and entry box side by side without using grid
    name_frame = tk.Frame(window, width=380)
    name_frame.pack()
    name_entry = tk.Entry(name_frame)
    name_entry.pack(side="right")
    tk.Label(name_frame, text="Enter a first name: ", bg="light green").pack(side="right")

    age_frame = tk.Frame(window, width=380)
    age_frame.pack()
    age_entry = tk.Entry(age_frame)
    age_entry.pack(side="right")
    tk.Label(age_frame, text="What is their age? ", bg="light green").pack(side="right")

    tk.Button(window, text="Submit", command=add_submit).pack(pady=2)
    add_name_label = tk.Label(window, text=" ")
    add_name_label.pack()

    quit_button = tk.Button(window, text="Exit Program", bg="#ff7070", activebackground="red", width=15, command=window.quit)
    quit_button.pack(side="bottom", pady=10)

    window.mainloop()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()


